As defined by section 4.2 of D6.1, the Security Orchestrator, for a given input security policy, defined in MSPL, automates the selection, (re-)deployment and/or (re-)configuration of security controls (aka policy enforcement enablers) and (possibly) associated monitoring enablers,
which, when all composed together, achieve the enforcement of that policy. In the context of FogProtect, the security orchestration does not only implement the concept of policy-driven security, but also software-defined security as it handles exclusively softwarized, indeed cloud-native security functions.

The Security Orchestrator's User Manual is on the project's [wiki](https://gitlab.com/fogprotecth2020/wp6/security-orchestrator/-/wikis/home).
 
The Security Orchestrator requires a Kubernetes environment for deployment. In this regard, the FogProtect Kubernetes environment setup guide is [here](k8s_deployment_environment_setup.md).
