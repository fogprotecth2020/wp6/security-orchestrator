<?xml version="1.1" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:mspl="urn:um.es:anastacia-framework:mspl-thales:xmlns:1"
                xmlns:xslt="http://www.w3.org/1999/XSL/Transform"
                xpath-default-namespace="http://www.w3.org/2005/xpath-functions"
                xmlns:map="http://www.w3.org/2005/xpath-functions/map"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <xsl:output encoding="UTF-8" method="text" omit-xml-declaration="yes"/>

    <!-- No output by default -->
    <xsl:template match="/">
        <xsl:apply-templates
                select="//mspl:configurationRuleAction[@xsi:type='AuthorizationAction']/mspl:AuthorizationTarget"/>
    </xsl:template>

    <xsl:template match="mspl:AuthorizationTarget">
        <xsl:variable name="opaVars" as="map(xs:string, xs:string)">
            <xsl:map>
                <xsl:for-each select="tokenize(., '\n\r?')">
                    <xsl:analyze-string select="." regex="^\s*(lockdown|cleared_by_operator)\s*:=\s*(true|false)\s*$">
                        <xsl:matching-substring>
                            <xsl:variable name="varName" select="regex-group(1)"/>
                            <xsl:message>
                                <xsl:text>### varName = </xsl:text><xsl:value-of select="$varName"/>
                            </xsl:message>
                            <xsl:map-entry key="$varName" select="regex-group(2)"/>
                        </xsl:matching-substring>
                    </xsl:analyze-string>
                </xsl:for-each>
            </xsl:map>
        </xsl:variable>

        <xsl:variable name="lockdown" select="map:get($opaVars, 'lockdown')"/>
        <xsl:message>
            <xsl:text>### lockdown = </xsl:text><xsl:value-of select="$lockdown"/>
        </xsl:message>
        <xsl:variable name="cleared_by_operator" select="map:get($opaVars, 'cleared_by_operator')"/>
        <xsl:message>
            <xsl:text>### cleared_by_operator = </xsl:text><xsl:value-of select="$cleared_by_operator"/>
        </xsl:message>
        <xsl:variable name="tag">
            <xsl:choose>
                <xsl:when test="$lockdown = 'true'">unsafe</xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="$cleared_by_operator = 'true'">safe</xsl:when>
                        <xsl:otherwise>semisafe</xsl:otherwise>
                    </xsl:choose>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:text disable-output-escaping="yes">
apiVersion: katalog.m4d.ibm.com/v1alpha1
kind: Asset
metadata:
  name: status
spec:
  assetDetails:
    dataFormat: csv
    connection:
      type: s3
  assetMetadata:
    tags:
    - </xsl:text><xsl:value-of select="$tag"/><xsl:text disable-output-escaping="yes">
    componentsMetadata:
      Status:
        tags:
        - </xsl:text><xsl:value-of select="$tag"/>

    </xsl:template>

</xsl:stylesheet>